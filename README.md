# 可视化网盘系统

- 开发者: 张津瑞
- 项目名称： 可视化网盘系统 (基于开源项目：奇文网盘)
- Email：jinrui.zhang@accenture.com

## 功能介绍

1. 多文件格式分类查看

2. 支持网格、表格视图、时间线三种展示视图

3. 支持极速秒传功能，提高上传效率

4. 多人上传同一文件，可多人并行上传，共享他人上传进度，极大提高上传效率

5. 拒绝冗余，每份文件只存一份，提高硬盘使用效率

6. 上传文件前台实时显示上传文件进度，上传速率，百分比等信息

7. 安全的下载机制，断点下载，权限校验，他人拿到了下载地址也无法下载您的文件

8. 支持视频音频播放，进度条拖拽，倍速播放

9. 拥有回收站功能

10. 高效的垃圾回收机制

11. 响应式前端布局

## 软件架构

该项目采用前后端分离的方式进行开发和部署,主要用到以下关键技术

**前端**：Element UI、Vue CLI@3、Node.js、Webpack

**后台**：Spring Boot、Spring Data Jpa、Spring Security

**开发工具**：Jetbrain 全家桶 idea datagrip webstorm

**项目管理工具**：maven

**数据库** : MySQL、H2

**数据结构**：递归算法，树的遍历和插入...

**设计模式**：工厂模式、单例模式...

## 源码地址

| 项目名称       | 源码地址                                                     |
| -------------- | ------------------------------------------------------------ |
| 可视化网盘前端 | [https://gitee.com/lnbxzjr/graduation-project-front-end](https://gitee.com/lnbxzjr/graduation-project---front-end.git) |
| 可视化网盘后台 | [https://gitee.com/lnbxzjr/graduation-project](https://gitee.com/lnbxzjr/graduation-project.git) |

## 线上访问地址

- http://disk.jerry-cloud.top

## 系统详细功能分析

### 登录功能实现

#### 前端程序实现逻辑

- 登录功能前端组件实现逻辑脑图

![登录功能前端组件实现逻辑脑图](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/20211211/登录组件.3rci9kpr5so0.png)

#### 后端程序实现逻辑

- 登录API实现逻辑脑图

![登录功能接口(userlogin)](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/20211214/登录功能接口(userlogin).5wqqrc8ghzo0.png)

- 忘记密码1，发送邮件获取令牌API实现逻辑脑图

![忘记密码功能接口（发送密码重置邮件）](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/忘记密码功能接口（发送密码重置邮件）__user_reset_passwd.4fu5jpg3ya00.webp)

- 忘记密码2，修改密码API实现逻辑脑图

![修改密码功能接口__user_reset_passwd](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/修改密码功能接口__user_reset_passwd.41kkdc8vfyg0.webp)

### 注册功能实现

#### 前端程序实现逻辑

- 注册功能前端组件实现逻辑脑图

![注册组件](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/20211214/注册组件.1y6wbnmgb674.png)

#### 后端程序实现逻辑

- 获取注册令牌功能API实现逻辑脑图

![用户注册接口：_user_register](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/用户注册接口：_user_register.7d2uo9677us0.webp)

### 上传功能实现逻辑

#### 上传功能实现脑图

![上传功能](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/20220102/上传功能.32f3tzlbgva0.webp)



### 新建文件夹功能实现逻辑

#### 新建文件夹功能实现脑图

![新建文件夹](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/20220102/新建文件夹.74cnitthka80.webp)

### 批量删除功能实现逻辑

#### 批量删除功能实现脑图

![批量删除](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/20220102/批量删除功能.3zksa4r5j2a0.webp)

### 批量移动功能实现逻辑

#### 批量移动功能实现脑图

![批量移动功能](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/20220102/批量移动功能.4flaivfitzq0.webp)

### 批量下载功能实现逻辑

#### 批量下载功能实现脑图

![批量下载功能](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/20220102/批量下载功能.67vsc8oicgg0.webp)

### 搜索功能实现逻辑

#### 搜索功能思维笔记

![](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/20220103/搜索功能.vaxpy4ge3fk.webp)

### 重命名按钮功能实现逻辑

#### 重命名按钮功能脑图

![](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/重命名.webp)

### 删除按钮功能实现逻辑

#### 删除按钮功能脑图

![删除按钮功能](https://www.hualigs.cn/image/61d55ef458283.jpg)

### 移动按钮功能实现逻辑

#### 移动按钮功能脑图

![移动](https://www.hualigs.cn/image/61d565dfd133b.jpg)

### 获取文件列表数据功能实现逻辑

#### 获取文件列表数据功能脑图

![获取文件列表数据功能](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/获取文件列表数据功能.5kdaio0r9l00.webp)

### 恢复文件功能实现逻辑

#### 恢复文件功能脑图

![获取文件列表数据功能](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/获取文件列表数据功能.37gdmt3ptog0.webp)

### 批量恢复文件功能实现逻辑

#### 批量恢复文件功能脑图

![批量恢复文件](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/获取文件列表数据功能-(1).45gs12h46t40.webp)

### 删除回收站文件功能实现逻辑

#### 删除回收站文件功能脑图

![删除回收站文件](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/获取文件列表数据功能-(1).1g0lrk2fjd28.webp)

### 设置头像功能实现逻辑

#### 设置头像功能脑图

![加载头像](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/批量恢复文件功能.1pjnij4kmikg.webp)

### 设置用户登录信息实现逻辑

#### 设置用户登录信息功能脑图

![设置用户登录信息](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/批量恢复文件功能-(1).niphqutrc40.webp)



- VueX核心之Mutation

> 更改 Vuex 的 store 中的状态的唯一方法是提交 mutation。Vuex 中的 mutation 非常类似于事件：每个 mutation 都有一个字符串的 **事件类型 (type)** 和 一个 **回调函数 (handler)**。这个回调函数就是我们实际进行状态更改的地方，并且它会接受 state 作为第一个参数：

```javascript
const store = new Vuex.Store({
  state: {
    count: 1
  },
  mutations: {
    increment (state) {
      // 变更状态
      state.count++
    }
  }
})
```



- VueX核心之Action

> ​	Action 类似于 mutation，不同在于：
>
> - Action 提交的是 mutation，而不是直接变更状态。
> - Action 可以包含任意异步操作。

```javascript
const store = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++
    }
  },
  actions: {
    increment (context) {
      context.commit('increment')
    }
  }
})
```

### VueX学习笔记

#### 要解决的问题

> 实现全局组件的互相通讯

#### 核心概念



![核心概念](https://vuex.vuejs.org/vuex.png)

- state => data:全局的状态

>
>
>

- Getters => computed

>
>
>

- Mutations => method

>
>
>

- Actions=>异步的method:可以发起异步的ajax请求

>
>
>

- Modules：模块

>​
>
>

## 部分功能截图

### 图片预览

![图片预览](https://cdn.jsdelivr.net/gh/neusoftzhangjinrui/image-hosting@master/20211210/图片查看.1k62mimcx79c.png)

### 视频播放

![视频播放](https://s2.loli.net/2021/12/08/gTZNAXQOt4uKBMc.png)

